# Infraestrutura

Este projeto possui a configuração necessária para provisionar containers com os serviços do RabbitMQ, LogStash, Elastic Search e Kibana. Para inicia-los execute o seguinte comando:

```
docker-compose up -d
```
